import unittest
from own_search.functions import search_combination_add
from own_search.functions import sum_list
from own_search.functions import move_circular

class Test_Own_Search(unittest.TestCase):
    
    def test_search_combination_add(self):
        data_test = [45,25,85,35,20,50]
        search_out = [[25,45],[20,50]]
        goal = 70
        search = search_combination_add(data_test, goal)
        self.assertEqual(search_out, search)
    
    def test_sum_list(self):
        data_test_1 = [45,25,85,35,20,50]
        data_test_2 = [66,55]
        data_test_3 = [789,1,-2,0.5,1.5]
        data_out_1 = 260
        data_out_2 = 121
        data_out_3 = 790
        self.assertEqual(sum_list(data_test_1), data_out_1)
        self.assertEqual(sum_list(data_test_2), data_out_2)
        self.assertEqual(sum_list(data_test_3), data_out_3)

    def test_move_circular(self):
        self.assertEqual(move_circular(9, 2, 10), 1)
        self.assertEqual(move_circular(9, 2, 0, 10), 1)
        self.assertEqual(move_circular(10, 3, 10, -5), -2)
        self.assertEqual(move_circular(9, 5, 10, 5), 9)
        self.assertEqual(move_circular(1, -2, 10), 9)