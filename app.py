from own_search.functions import search_combination_add
from own_csv.read_csv import read_csv
from data.config_patern import data_search, pattern
from own_convert.functions import convert

data = read_csv('csv/probAlgoTriMontantAsc.csv', ',')
data_fetch = []
data_fetch_id = []
for row in data:
    #print(row['Montant'])
    #print(convert(row['Montant'], pattern))
    data_fetch.append(convert(row['Montant'], pattern))
    data_fetch_id.append(row['ID'])

row = data_search[0]
data_row = search_combination_add(data_fetch, row)
if len(data_row) > 0:
    data_rew = []
    for rew in data_row:
        entry = []
        for ruw in rew:
            entry.append(data_fetch_id[data_fetch.index(ruw)])
        data_rew.append(entry.copy())
    print("Pour la donnée : "+str(row)+f" Nous avons trouvé les {len(data_rew[0])} données suivantes :")
    print(data_row)
    print("Avec les ids suivantes :")
    print(data_rew)
    data = read_csv('csv/probAlgoTriMontantDesc.csv', ',')
    data_fetch = []
    data_fetch_id = []
    for row in data:
        data_fetch.append(convert(row['Montant'], pattern))
        data_fetch_id.append(row['ID'])
    for rowdata in data_row[0]:
        data_fetch.remove(rowdata)
    for rewdata in data_rew[0]:
        data_fetch_id.remove(rewdata)
else:
    print("Pour la donnée : "+str(row)+" Nous n'avons rien trouvé.")
input()
row = data_search[1]
data_row = search_combination_add(data_fetch, row)
if len(data_row) > 0:
    data_rew = []
    for rew in data_row:
        entry = []
        for ruw in rew:
            entry.append(data_fetch_id[data_fetch.index(ruw)])
        data_rew.append(entry.copy())
    print("Pour la donnée : "+str(row)+f" Nous avons trouvé les {len(data_rew[0])} données suivantes :")
    print(data_row)
    print("Avec les ids suivantes :")
    print(data_rew)
    for rowdata in data_row[0]:
        data_fetch.remove(rowdata)
    for rewdata in data_rew[0]:
        data_fetch_id.remove(rewdata)
else:
    print("Pour la donnée : "+str(row)+" Nous n'avons rien trouvé.")