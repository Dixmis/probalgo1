import os
"""
    Fonction convert
    %       Cette fonction recherche toutes les combinaisons pouvant
            atteindre le goal (tout en évitant la redondance de combinaison
            et en triant les combinaisons).
    %IN     data : Liste de données à utiliser,
            goal : Donnée à trouver
    %OUT    out : Liste des combinaisons possibles
"""
def search_combination_add(data, goal):
    out = []
    #print("0")
    for i in range(len(data)):
        list_index = []
        entry = []
        #data_temp = reorder_list(data, start=i)
        entry = test_recursif(data, entry, i-1, goal, list_index)
        if entry != False:
            entry.sort()
            if not(entry in out):
                out.append(entry.copy())
    return out
    """
    for i in range(len(data)):
        entry = []
        if data[i] <= goal:
            entry.append(data[i])
            for j in range(len(data)):
                if i != j:
                    if sum_list(entry) + data[move_circular(i, j, len(data)-1)] <= goal:
                        entry.append(data[j])
        if sum_list(entry) == goal:
            entry.sort()
            if not(entry in out):
                out.append(entry.copy())
    if sum_list(entry) == goal:
        entry.sort()
        if not(entry in out):
            out.append(entry.copy())
    return out
    """


def test_recursif(data, entry, index, goal, list_index):
    if sum_list(entry) == goal:
        return entry
    for i in range(index + 1, len(data)):
        if sum_list(entry) + data[i] <= goal:
            entry.append(data[i])
            list_index.append(i)
            #print(list_index)
            res = test_recursif(data, entry, i, goal, list_index)
            if res != False:
                return res
            else:
                entry.pop()
                list_index.pop()
        else:
            return False
    return False
        

def reorder_list(data, start=1):
    data_out = []
    for i in range(start, len(data)):
        data_out.append(data[i])
    for i in range(len(data)-len(data_out)):
        data_out.append(data[i])
    return data_out 

"""
    Fonction sum_list
    %       Cette fonction nous retourne la somme de tout les éléments
            d'une liste.
    %IN     list_data : La liste
    %OUT    out : La somme
"""
def sum_list(list_data):
    out = 0
    for data in list_data:
        out = out + data
    return out

"""
    Fonction move_circular
    %       Cette fonction renvoie une valeur comprise entre les
            bornes mini et maxi en effectuant un mouvement circulaire au
            besoin.
    %IN     number : nombre initial,
            move : le déplacement souhaité,
            maxi : le maximum de l'intervale
            mini : le minimum de l'intervale
    %OUT    out : la valeur déplacée
"""
def move_circular(number, move, maxi, mini=0):
    out = number + move
    if maxi < mini:
        temp = maxi
        maxi = mini
        mini = temp
    while (out > maxi or out < mini):
        if out > maxi:
            out = mini + (out - maxi)
        if out < mini:
            out = maxi - (mini - out)
    return out